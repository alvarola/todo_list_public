class Item < ActiveRecord::Base
  belongs_to :project

  def toggle!
    self.done = !done?
    self.save
  end
end
