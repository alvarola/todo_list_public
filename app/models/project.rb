class Project < ActiveRecord::Base
  has_many :items

  def done?
    items.all? { |item| item.done? }
  end

  def recent_items
    items.where(created_at: [Time.now.beginning_of_day..Time.now.at_end_of_day])
  end

  def items_by_days
    items.group_by { |item| item.created_at.to_date }
  end

  def to_s
    self.name
  end
end
