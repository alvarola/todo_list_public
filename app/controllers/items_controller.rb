class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy, :toggle]

  def index
    @items = Item.all
  end

  def show
  end

  def new
    @item = Item.new
  end

  def toggle
    @item.toggle!
    respond_to do |format|
      format.js {}
    end
  end

  def filter
    project_id = item_filter_params[:project_id]
    done = item_filter_params[:done]
    @items = project_id.present? ? Item.where(project_id: project_id) : Item.all
    @items = done.present? ? @items.where(done: done) : @items
    respond_to do |format|
      format.js {}
    end
  end

  def edit
  end

  def create
    @item = Item.new(item_params)

    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, notice: 'Item was successfully created.' }
        format.json { render :show, status: :created, location: @item }
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:description, :done, :project_id)
    end

    def item_filter_params
      params.require(:item_filter).permit(:project_id, :done)
    end

end
