Rails.application.routes.draw do
  root to: 'items#index'
  resources :projects
  post "items/filter", to:"items#filter"
  resources :items do
    member do
      get 'toggle'
    end
  end
end
