# Tasks #

All open source gems, repos and internets are at your disposal for the excersise.
You have to use Ruby (1.9 or higher) and Rails (3.2 or higher) but all the remaining
technology decisions are yours. Good luck!

## 1) CRUD ##
Make a web application to manage a TODO list. Every (TODO) Item is just a piece of text that
is either `opened` or `closed`. The user has to be able to create an item, update an item
and list all of them.

## 2) Tests ##
Improve the data model with those features:

* add `Project` model with name and description attributes
* every `Item` `belongs_to` a `Project`
* `Project#items` => all items of the project
* `Project#done?` => true if all its items are `closed`
* `Project#items_by_days` => hash of items grouped by day
* `Project#recent_items` => items that were created today

Cover it by unit tests.

## 3) AJAX ##
Improve the item listing so that:

* you can list items by project
* the items listing separates `opened` from `closed` tasks
* the user can close/open items by one click without a page refresh

Working app http://floating-mesa-3469.herokuapp.com/