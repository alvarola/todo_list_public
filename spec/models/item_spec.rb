require 'rails_helper'

describe Item do
  describe "#toggle!" do
    context "item is open" do
      before(:each) do
        subject.done = false
        subject.toggle!
      end

      specify { expect(subject.done?).to eq(true) }
    end
    context "items is closed" do
      before(:each) do
        subject.done = true
        subject.toggle!
      end

      specify { expect(subject.done?).to eq(false) }
    end
  end
end
