require 'rails_helper'

describe Project do

  context "date constrained methods" do
    let(:item_from_yesterday) { Item.create description: "Past item", created_at: Date.yesterday }
    let(:today_item_creation_date) { Date.today }
    let(:item_from_today) { Item.create description: "Present item", created_at: today_item_creation_date }

    before(:each) do
      subject.items << item_from_yesterday
      subject.items << item_from_today
      subject.save
    end

    describe "#recent_items" do
      specify { expect(subject.recent_items.count).to eq(1) }
      specify { expect(subject.recent_items.first).to eq(item_from_today) }
    end

    describe "#items_by_days" do
      specify { expect(subject.items_by_days.count).to eq(2) }
      specify { expect(subject.items_by_days[today_item_creation_date]).to eq([item_from_today]) }
    end
  end

  describe "#done?" do
    let(:item_open) { Item.new description: "Item 1", done: false }
    let(:item_closed) { Item.new description: "Item 2", done: true }

    context "when there are no items" do
      before(:each) do
        subject.items = []
      end

      specify { expect(subject.done?).to eq(true) }
    end

    context "when only one item is done" do
      before(:each) do
        subject.items << item_open
        subject.items << item_closed
      end

      specify { expect(subject.done?).to eq(false) }
    end

    context "when all items are done" do
      before(:each) do
        subject.items << item_closed
        subject.items << Item.new(description: "Item 3", done: true)
      end

      specify { expect(subject.done?).to eq(true) }
    end
  end
end
